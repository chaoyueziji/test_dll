#ifndef MAX_H_
#define MAX_H_
#ifdef _cplusplus
extern "C"
{
#endif
    int max(int a, int b);
#ifdef cplusplus
}
/* code */
#endif //_cplusplus

#endif //MAX_H_
